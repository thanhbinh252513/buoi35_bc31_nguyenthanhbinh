import React, { Component } from "react";
import { shoeArr } from "./DataShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";
import TableShoe from "./TableShoe";

export default class Ex_ShoeShopRedux extends Component {
  handleRemoveToCart = (idShoe) => {
    // Dùng findIndex để tìm ra index của sản phẩm trong mảng gioHang
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    // Tìm thấy khi index!=-1 thì dùng splice để loại bỏ item trong mảng theo index
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({
        gioHang: cloneGioHang,
      });
    }
  };
  handleChangeQuantity = (idShoe, soLuong) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index !== -1) {
      cloneGioHang[index].quantity = cloneGioHang[index].quantity + soLuong;
    }
    if (cloneGioHang[index].quantity < 1) {
      cloneGioHang.splice(index, 1);
    }
    console.log(cloneGioHang);
    this.setState({
      gioHang: cloneGioHang,
    });
  };
  render() {
    return (
      <div className="container py-5">
        <TableShoe />
        <ListShoe />
      </div>
    );
  }
}
