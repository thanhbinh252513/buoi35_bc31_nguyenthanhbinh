import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CHANGE_QUANTITY,
  DELETE_TO_CART,
} from "./redux/constant/shoeShopConstant";

class TableShoe extends Component {
  renderTable = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
            >
              -
            </button>
            <span className="mx-2">{item.soLuong}</span>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
            >
              +
            </button>
          </td>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.handleDeleteToCart(item.id);
            }}
          >
            Xóa
          </button>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeShopReducer.gioHang,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleDeleteToCart: (id) => {
      dispatch({
        type: DELETE_TO_CART,
        payload: id,
      });
    },
    handleChangeQuantity: (id, soLuong) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: {
          id: id,
          soLuong: soLuong,
        },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TableShoe);
