/* eslint-disable no-fallthrough */
/* eslint-disable no-undef */
import { shoeArr } from "../../DataShoe";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  DELETE_TO_CART,
} from "../constant/shoeShopConstant";

let initialState = {
  shoeArr: shoeArr,
  gioHang: [],
};

export let shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.gioHang.findIndex((item) => {
        return item.id === payload.id;
      });
      // Th1: Chưa có sản phẩm trong cửa hàng => thêm key soLuong vào item
      // và để soLuong có value là 1
      // và thêm item vào gioHang và set lại gioHang
      let cloneGioHang = [...state.gioHang];
      if (index === -1) {
        let newSp = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSp);
        // th1
      } else {
        cloneGioHang[index].soLuong++;
        //th2
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case DELETE_TO_CART: {
      let index = state.gioHang.findIndex((item) => {
        return item.id === payload;
      });
      console.log("index:", index);
      // Tìm thấy khi index!=-1 thì dùng splice để loại bỏ item trong mảng theo index
      if (index !== -1) {
        let cloneGioHang = [...state.gioHang];
        cloneGioHang.splice(index, 1);
        state.gioHang = cloneGioHang;
        return { ...state };
      }
    }
    case CHANGE_QUANTITY: {
      let index = state.gioHang.findIndex((item) => {
        return item.id === payload.id;
      });
      let cloneGioHang = [...state.gioHang];
      if (index !== -1) {
        cloneGioHang[index].soLuong =
          cloneGioHang[index].soLuong + payload.soLuong;
      }
      if (cloneGioHang[index].soLuong < 1) {
        cloneGioHang.splice(index, 1);
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
