import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";
class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ItemShoe key={item.id} shoeData={item} handleAddToCart={() => {}} />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    shoeArr: state.shoeShopReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
